const fs = require('fs')

function themeRoot () {
  var isLiferayTheme = false
  if (fs.existsSync(process.cwd() + '/package.json')) {
    var packagePath = process.cwd() + '/package.json'
    var packageFile = JSON.parse(fs.readFileSync(packagePath, 'utf8'))
    Object.keys(packageFile).forEach(function (key) {
      if (packageFile[key].toString() === 'liferay-theme') {
        isLiferayTheme = true
        return isLiferayTheme
      }
    })
  }
  return {
    isLiferayTheme: isLiferayTheme,
    packagePath: packagePath
  }
}

module.exports = themeRoot()
