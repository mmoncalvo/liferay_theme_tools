function cursorControl () {
  const ESC = '\u001b['
  return {
    writeText: function (text) {
      return process.stdout.write(text)
    },
    cursorTo: function (x, y) {
      if (typeof x !== 'number') {
        return process.stdout.write('The `x` argument is required')
      }
      if (typeof y !== 'number') {
        return [ESC] + (x + 1) + 'G'
      }
      return process.stdout.write([ESC] + (y + 1) + ';' + (x + 1) + 'H')
    },
    cursorUp: function (n) {
      return process.stdout.write([ESC] + [n] + 'A')
    },
    cursorDown: function (n) {
      return process.stdout.write([ESC] + [n] + 'B')
    },
    cursorForward: function (n) {
      return process.stdout.write([ESC] + [n] + 'C')
    },
    cursorBackward: function (n) {
      return process.stdout.write([ESC] + [n] + 'D')
    },
    cursorLeft: function () {
      return process.stdout.write([ESC] + 'G')
    },
    cursorNextLine: function () {
      return process.stdout.write([ESC] + 'E')
    },
    cursorPrevLine: function () {
      return process.stdout.write([ESC] + 'F')
    },
    eraseLine: function () {
      return process.stdout.write([ESC] + '2K')
    },
    eraseScreen: function () {
      return process.stdout.write([ESC] + '2J')
    },
    clearScreen: function () {
      return process.stdout.write('\u001Bc')
    },
    clearUp: function () {
      return process.stdout.write([ESC] + '1J')
    },
    clearDown: function () {
      return process.stdout.write([ESC] + '0J')
    },
    eraseStartLine: function () {
      return process.stdout.write([ESC] + '1K')
    },
    eraseEndLine: function () {
      return process.stdout.write([ESC] + 'K')
    },
    cursorSavePosition: function () {
      return process.stdout.write([ESC] + 's')
    },
    cursorRestorePosition: function () {
      return process.stdout.write([ESC] + 'u')
    },
    cursorShow: function () {
      return process.stdout.write([ESC] + '?25h')
    },
    cursorHide: function () {
      return process.stdout.write([ESC] + '?25l')
    }
  }
}
module.exports = cursorControl()
