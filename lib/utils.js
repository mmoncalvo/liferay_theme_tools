#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const colors = require('colors')
const cc = require('../lib/cursorControl')
const { table } = require('table')
const tgbc = require('table').getBorderCharacters
const emoji = require('node-emoji')
const APP_ROOT = require('../appRoot')
const THEME_ROOT = require('../themeRoot')
const colorwd = colors.white.dim
const cgreen = colors.green
// const colorcdb = colors.cyan.dim.bold
// const colorm = colors.magenta
// const colormd = colors.magenta.dim
// const colory = colors.yellow
const colorg = colors.gray
const colorr = colors.red

const utils = module.exports

utils.dev = true
utils.APP_ROOT = APP_ROOT
utils.THEME_ROOT = THEME_ROOT

utils.screenSettings = function () {
  cc.cursorTo(0, 0)
  cc.clearScreen()
  cc.eraseLine()
}

utils.cmdOutput = function (text) {
  function setEscapes (callback) {
    var escapes = function () {
      cc.writeText(text)
      cc.cursorDown(1)
      cc.cursorLeft()
      // console.log('escapes ->')
    }
    var space = function () {
      cc.cursorDown(1)
      cc.cursorLeft()
      // console.log('space ->')
    }
    if (typeof text === 'undefined') {
      return callback(space())
    } else return callback(escapes())
  }
  setEscapes(function (setescape) {
    return setescape
  })
}

utils.devInfo = function (info) {
  if (utils.dev === true) {
    var showInfo = function (output) {
      const rocket = emoji.find('rocket')
      output = [rocket.emoji, ' '] + colors.magenta(info)
      cc.writeText(output)
      cc.cursorDown(1)
      cc.cursorLeft()
    }
    if (info !== 'undefined') return showInfo()
  }
}

utils.fileConfig = function () {
  var deployConfig = function () {
    var filePath = path.resolve(APP_ROOT, './config/deploy.json')
    var fileConf = JSON.parse(fs.readFileSync(filePath, 'utf8'))
    return {
      path: filePath,
      get: fileConf
    }
  }
  var resourcesConfig = function () {
    var filePath = path.resolve(APP_ROOT, './config/resources.json')
    var fileConf = JSON.parse(fs.readFileSync(filePath, 'utf8'))
    return {
      path: filePath,
      get: fileConf
    }
  }
  return {
    deployconf: deployConfig(),
    resourcesconf: resourcesConfig()
  }
}

utils.filePackage = function () {
  var noPackageThemeFound = function () {
    const data = [
      [colorr('No liferay theme package.json found !')]
    ]
    const config = {
      border: tgbc('norc'),
      columnCount: 1,
      columns: {
        0: {
          paddingLeft: 5,
          paddingRight: 5,
          alignment: 'center'
        }
      }
    }
    return utils.cmdOutput(colorg(table(data, config)))
  }
  if (utils.THEME_ROOT.isLiferayTheme) {
    var packagePath = utils.THEME_ROOT.packagePath
    var packageFile = JSON.parse(fs.readFileSync(packagePath, 'utf8'))
    const data = [
      [colorwd('Theme name:'), cgreen(packageFile.name)],
      [colorwd('Version:'), cgreen(packageFile.version)],
      [colorwd('Base theme:'), cgreen(packageFile.liferayTheme.baseTheme)],
      [colorwd('Language:'), cgreen(packageFile.liferayTheme.templateLanguage)]
    ]
    const config = {
      border: {
        bodyJoin: '│',
        bodyLeft: '',
        bodyRight: ''
      },
      columnCount: 2,
      columns: {
        0: {
          paddingRight: 2
        },
        1: {
          paddingLeft: 2
        }
      },
      drawHorizontalLine: () => {
        return false
      }
    }
    utils.cmdOutput(colorg(table(data, config)))
    utils.cmdOutput()
  } else {
    return noPackageThemeFound()
  }
}

utils.traceEvents = function () {
  (function () {
    var EventEmitter = require('events').EventEmitter
    var inspect = require('util').inspect
    var emit_ = EventEmitter.prototype.emit
    EventEmitter.prototype.emit = function (name) {
      var args = Array.prototype.slice.call(arguments)
      args.toString()
      if (!(this === process.stderr && name === 'drain')) {
        console.error('')
        console.error(
          colors.magenta.italic('Event') +
          colors.white.dim(' -> ') + '%s' +
          colors.magenta.italic(' arguments') +
          colors.white.dim(' -> ') + '%s',
          colors.cyan(name),
          colors.cyan(inspect(args.slice(1), false, 1))
        )
      }
      return emit_.apply(this, args)
    }
  })()
}
