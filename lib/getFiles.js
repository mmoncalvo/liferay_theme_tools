const path = require('path')
const fs = require('fs')

function getFiles (startPath, filter) {
  var results = []
  var filesize = []

  if (!fs.existsSync(startPath)) {
    console.log('no dir ', startPath)
    return
  }

  function getFileSize (filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    const fileSizeInMegabytes = fileSizeInBytes / 1000000.0
    return fileSizeInMegabytes
  }

  var files = fs.readdirSync(startPath)
  for (var i = 0; i < files.length; i++) {
    var filename = path.join(startPath, files[i])
    var fullPath = path.join(startPath, files[i])
    var size = getFileSize(fullPath)
    var stat = fs.lstatSync(filename)
    if (stat.isDirectory()) {
      results = results.concat(getFiles(filename, filter))
    } else if (filename.indexOf(filter) >= 0) {
      results.push(filename)
      filesize.push({
        filename: path.basename(filename),
        filesize: size
      })
    }
  }
  return {
    FILES: results,
    SIZES: filesize
  }
}

module.exports = getFiles
