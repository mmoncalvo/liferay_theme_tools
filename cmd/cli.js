#!/usr/bin/env node
const fs = require('fs')
const resources = require('../cmd/resources')
const { table } = require('table')
const tgbc = require('table').getBorderCharacters
const deploy = require('../cmd/deploy')
const colors = require('colors')
const utils = require('../lib/utils')
const inquirer = require('inquirer')
const colorcdb = colors.cyan.dim.bold
const cmdOutput = utils.cmdOutput
const colormd = colors.magenta.dim
const colorwd = colors.white.dim
const colorm = colors.magenta
const colory = colors.yellow
const colorc = colors.cyan
const colorg = colors.gray
const colorr = colors.red

function start () {
  utils.screenSettings()
  var output = '\n'
  output += '##     ########   ########  #######   #######  ##       ######   \n'
  output += '##        ##         ##    ##    ### ##    ### ##      ##        \n'
  output += '##        ##   ##    ##    ##    ### ##    ### ##       ######   \n'
  output += '##        ##         ##    ##    ### ##    ### ##           ###  \n'
  output += '#######   ##         ##     #######   #######  #######  ######   \n'
  output += '\n'
  cmdOutput(colorcdb(output))
  utils.filePackage()
  SelectOption()
}

function main () {
  start()
}

var SelectOption = function () {
  inquirer.prompt({
    type: 'list',
    name: 'mainmenu',
    message: colormd('Main Options'),
    choices: ['Deploy', 'Resources']
  }).then(function (answers) {
    if (answers.mainmenu === 'Deploy') {
      return deployOptions()
    } else if (answers.mainmenu === 'Resources') {
      return resourcesOptions()
    }
  })
}

function deployOptions () {
  inquirer.prompt({
    type: 'list',
    name: 'deploymenu',
    message: colormd('Select a Deploy option'),
    choices: [{
      value: 'desa',
      name: 'Deploy to desa'
    },
    {
      value: 'prep',
      name: 'Deploy to prep',
      response: 'string'
    },
    {
      value: 'conf',
      name: 'Config file',
      response: 'string'
    },
    new inquirer.Separator(),
    {
      value: 'mainmenu',
      name: colorm('<< Menu')
    }
    ]
  }).then(function (answers) {
    if (answers.deploymenu === 'desa') {
      goDeploy()
    } else if (answers.deploymenu === 'prep') {
      goDeploy()
    } else if (answers.deploymenu === 'conf') {
      deployConfigOptions()
    } else if (answers.deploymenu === 'mainmenu') {
      start()
    }
    function goDeploy () {
      module.exports.deploymenu = answers.deploymenu
      return deploy(answers.deploymenu)
    }
  })
}

function resourcesOptions () {
  inquirer.prompt({
    type: 'list',
    name: 'resourcesmenu',
    message: colormd('Resources options'),
    choices: [{
      value: 'resourceslist',
      name: 'List Resources'
    },
    {
      value: 'resourceinstall',
      name: 'Install Resource'
    },
    {
      value: 'conf',
      name: 'Config file'
    },
    new inquirer.Separator(),
    {
      value: 'mainmenu',
      name: colorm('<< Menu')
    }
    ]
  }).then(function (answers) {
    if (answers.resourcesmenu === 'resourceslist') {
      return goResources()
    } else if (answers.resourcesmenu === 'resourceinstall') {
      return goResources()
    } else if (answers.resourcesmenu === 'conf') {
      return resourcesConfigOptions()
    } else if (answers.resourcesmenu === 'mainmenu') {
      return start()
    }
    function goResources () {
      module.exports.resourcesmenu = answers.resourcesmenu
      return resources(answers.resourcesmenu)
    }
  })
}

function deployConfigOptions () {
  const fileConfig = utils.fileConfig()
  const filePath = fileConfig.deployconf.path
  const getConf = fileConfig.deployconf.get
  const http_ = ' http://'
  const http = colorc(http_)
  function setConfigMenu () {
    inquirer.prompt({
      type: 'list',
      name: 'setConfigMenu',
      message: colory('Select a Config option'),
      choices: [{
        value: 'add',
        name: 'Add configuration'
      },
      {
        value: 'remove',
        name: 'Remove configuration'
      },
      new inquirer.Separator(),
      {
        value: 'deploymenu',
        name: colorm('<< Deploy Menu')
      }
      ]
    }).then(function (answers) {
      if (answers.setConfigMenu === 'add') {
        return setConfig()
      } else if (answers.setConfigMenu === 'remove') {
        removeConfig()
      } else if (answers.setConfigMenu === 'deploymenu') {
        deployOptions()
      }
    })
  }
  function removeConfig () {
    function optionsList () {
      var arrOptions = Object.keys(getConf.userconfig)
      if (arrOptions.length === 0) {
        var data = [
          [colorr('No configurations found !')]
        ]
        var config = {
          border: tgbc('norc'),
          columns: {
            0: {
              alignment: 'center',
              width: 40
            }
          }
        }
        cmdOutput(colorg(table(data, config)))
        deployConfigOptions()
      } else {
        inquirer.prompt({
          type: 'list',
          name: 'removelist',
          message: colormd('Remove config'),
          choices: arrOptions
        }).then(function (answers) {
          console.log('​optionsList -> key', answers)
          const copyGetConf = Object.assign({}, getConf)
          const copyUserConf = Object.assign({}, copyGetConf.userconfig)
          arrOptions.forEach(function (key) {
            if (answers.removelist === key) {
              delete copyUserConf[key]
            }
          })
          copyGetConf.userconfig = copyUserConf
          var newGetConf = JSON.stringify(copyGetConf, null, 4)
          fs.writeFile(filePath, newGetConf, 'utf8', function (err) {
            if (err) return console.error(err)
            deployConfigOptions()
          })
          if (answers.deploymenu === 'mainmenu') {
            main()
          }
        })
      }
    }
    optionsList()
  }
  function setConfig () {
    const noEmpty = function (value) {
      if (value) { return true }
      return 'no selected option'
    }
    inquirer.prompt([
      {
        type: 'input',
        name: 'setName',
        message: 'What\'s your config name?',
        validate: noEmpty
      },
      {
        type: 'input',
        name: 'setHost',
        message: 'What\'s your host?' + http,
        validate: noEmpty
      },
      {
        type: 'input',
        name: 'setUser',
        message: 'What\'s your user?',
        validate: noEmpty
      },
      {
        type: 'input',
        name: 'setPass',
        message: 'What\'s your pass?',
        validate: noEmpty
      },
      {
        type: 'confirm',
        name: 'fileSetAgain',
        message: 'Save the configuration',
        default: true
      }
    ]).then(function (answers) {
      if (answers.fileSetAgain) {
        var HTTP = 'http://'
        var data = [
          [colorwd('name:'), colorm(answers.setName)],
          [colorwd('host:'), colorm(HTTP) + colorm(answers.setHost)],
          [colorwd('user:'), colorm(answers.setUser)],
          [colorwd('pass:'), colorm(answers.setPass)]
        ]
        var config = {
          border: tgbc('norc'),
          columnDefault: {
            width: 30
          },
          columnCount: 2,
          columns: {
            0: {
              width: 5
            }
          }
        }
        cmdOutput('space')
        cmdOutput(colorcdb('Config'))
        cmdOutput(colorg(table(data, config)))
        var addConf = {
          [answers.setName]: {
            host: HTTP + answers.setHost,
            user: answers.setUser,
            pass: answers.setPass
          }
        }
        var copyGetConf = Object.assign({}, getConf)
        const newConf = Object.assign({}, copyGetConf.userconfig, addConf)
        copyGetConf.userconfig = newConf
        var newConfig = JSON.stringify(copyGetConf, null, 4)
        fs.writeFile(filePath, newConfig, 'utf8', function (err) {
          if (err) return console.error(err)
          deployConfigOptions()
        })
      } else {
        return setConfig()
      }
    })
  }
  return setConfigMenu()
}
function resourcesConfigOptions () {
  const fileConfig = utils.fileConfig()
  const filePath = fileConfig.resourcesconf.path
  const getConf = fileConfig.resourcesconf.get
  function setConfigMenu () {
    inquirer.prompt({
      type: 'list',
      name: 'setConfigMenu',
      message: colory('Select a Config option'),
      choices: [{
        value: 'addPath',
        name: 'Add Resources Path'
      },
      {
        value: 'remove',
        name: 'Remove configuration'
      },
      new inquirer.Separator(),
      {
        value: 'resourcesmenu',
        name: colorm('<< Resources Menu')
      }
      ]
    }).then(function (answers) {
      if (answers.setConfigMenu === 'addPath') {
        return setConfig()
      } else if (answers.setConfigMenu === 'remove') {
        removeConfig()
      } else if (answers.setConfigMenu === 'resourcesmenu') {
        resourcesOptions()
      }
    })
  }
  function removeConfig () {
    function optionsList () {
      var arrOptions = Object.keys(getConf.resources)
      if (arrOptions.length === 0) {
        var data = [
          [colorr('No configurations found !')]
        ]
        var config = {
          border: tgbc('norc'),
          columns: {
            0: {
              alignment: 'center',
              width: 40
            }
          }
        }
        cmdOutput(colorg(table(data, config)))
        resourcesConfigOptions()
      } else {
        inquirer.prompt({
          type: 'list',
          name: 'removelist',
          message: colormd('Remove config'),
          choices: arrOptions
        }).then(function (answers) {
          console.log('​optionsList -> key', answers)
          const copyGetConf = Object.assign({}, getConf)
          const copyUserConf = Object.assign({}, copyGetConf.resources)
          arrOptions.forEach(function (key) {
            if (answers.removelist === key) {
              delete copyUserConf[key]
            }
          })
          copyGetConf.resources = copyUserConf
          var newGetConf = JSON.stringify(copyGetConf, null, 4)
          fs.writeFile(filePath, newGetConf, 'utf8', function (err) {
            if (err) return console.error(err)
            resourcesConfigOptions()
          })
          if (answers.resourcesmenu === 'mainmenu') {
            main()
          }
        })
      }
    }
    optionsList()
  }
  function setConfig () {
    const noEmpty = function (value) {
      if (value) { return true }
      return 'no selected option'
    }
    inquirer.prompt([
      {
        type: 'input',
        name: 'setPath',
        message: 'What\'s your resources Path?',
        validate: noEmpty
      },
      {
        type: 'confirm',
        name: 'fileSetAgain',
        message: 'Save the configuration',
        default: true
      }
    ]).then(function (answers) {
      if (answers.fileSetAgain) {
        var data = [
          [colorwd('Path:'), colorm(answers.setPath)]
        ]
        var config = {
          border: tgbc('norc'),
          columnCount: 2,
          columns: {
            0: {
              width: 5,
              paddingLeft: 2
            },
            1: {
              paddingRight: 2,
              paddingLeft: 2
            }
          }
        }
        cmdOutput(colorcdb('Resources directory'))
        cmdOutput(colorg(table(data, config)))
        var addConf = {path: answers.setPath}
        var copyGetConf = Object.assign({}, getConf)
        const newConf = Object.assign({}, copyGetConf.resources, addConf)
        copyGetConf.resources = newConf
        var newConfig = JSON.stringify(copyGetConf, null, 4)
        fs.writeFile(filePath, newConfig, 'utf8', function (err) {
          if (err) return console.error(err)
          resourcesConfigOptions()
        })
      } else {
        return setConfig()
      }
    })
  }
  return setConfigMenu()
}

main()

module.exports.opts = {
  selectopt: function () {
    return SelectOption()
  },
  resourcesopt: function () {
    return resourcesOptions()
  },
  deployopt: function () {
    return deployOptions()
  }
}
