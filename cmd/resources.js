#!/usr/bin/env node

// const path = require('path')
const fs = require('fs')
// const path = require('path')
const colors = require('colors')
const inquirer = require('inquirer')
const utils = require('../lib/utils')
const copy = require('recursive-copy')
const cli = require('../cmd/cli')
const cmdOutput = utils.cmdOutput
const THEME_ROOT = utils.THEME_ROOT
const colormd = colors.magenta.dim
// const APP_ROOT = utils.appRoot()
// const colorwd = colors.white.dim
// const colorc = colors.cyan
// const colorcdb = colors.cyan.dim.bold
// const colorm = colors.magenta
// const colory = colors.yellow
// const colorg = colors.gray
// const colorr = colors.red

function list () {
  cmdOutput(colors.cyan('OPTION List Resources'))
  const fileConfig = utils.fileConfig()
  const getConf = fileConfig.resourcesconf.get
  var resourcesPath = getConf.resources.path

  function writeImports () {
    let lyrics = 'But still I\'m having memories of high speeds when the cops crashed\n' +
    'As I laugh, pushin the gas while my Glocks blast\n' +
    'We was young and we was dumb but we had heart'

    // write to a new file named 2pac.txt
    fs.writeFile(THEME_ROOT + '/' + '2pac.txt', lyrics, (err) => {
      // throws an error, you could also catch it here
      if (err) throw err

      // success case, the file was saved
      console.log('Lyric saved!')
    })
  }

  function resourcesList (resourcesArr) {
    inquirer.prompt({
      type: 'list',
      name: 'resourcelist',
      message: colormd('Select available resource'),
      choices: resourcesArr
    }).then(function (answers) {
      resourcesArr.forEach(function (resource) {
        Object.keys(resource).forEach(function (key) {
          if (answers.resourcelist === resource[key]) {
            cmdOutput(colors.cyan('OPTION List SELECTED', resource.name))
            var dest = './src'
            var src = resourcesPath + '/' + resource.name
            var options = {overwrite: true, filter: ['**/*', '!resource.json']}
            copy(src, dest, options).then(function (results) {
              console.info('Copied ' + results.length + ' files')
              // return writeImports()
            }).catch(function (error) {
              console.error('Copy failed: ' + error)
            })
          }
        })
      })
    })
  }

  function getResources (dir) {
    var resourcesArr = []
    fs.readdirSync(dir).forEach(function (resource) {
      var resourceRoot = dir + '/' + resource
      if (fs.statSync(resourceRoot).isDirectory()) {
        var resourceConfig = resourceRoot + '/resource.json'
        var getConfig = JSON.parse(fs.readFileSync(resourceConfig, 'utf8'))
        resourcesArr.push({
          value: getConfig.id,
          name: getConfig.name
        })
      }
    })
    resourcesList(resourcesArr)
  }
  getResources(resourcesPath)
}

function install () {
  return cmdOutput(colors.cyan('OPTION Install Resource'))
}

module.exports = function () {
  console.log('--------- RESOURCES ---------')
  if (cli.resourcesmenu === 'resourceslist') {
    return list()
  }
  if (cli.resourcesmenu === 'resourceinstall') {
    return install()
  }
}
