#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const cli = require('../cmd/cli')
const utils = require('../lib/utils')
const getfiles = require('../lib/getFiles')
const fileconfig = require('../config/deploy.json')
const progressBar = require('cli-progress')
const cc = require('../lib/cursorControl')
const scpClient = require('scp2')
const Client = scpClient.Client
const colors = require('colors')
const emoji = require('node-emoji')
const { table } = require('table')
const tgbc = require('table').getBorderCharacters
const events = require('events')
const APP_ROOT = utils.APP_ROOT
class RunClient {
  constructor () {
    utils.dev = true
    if (utils.dev === false) {
      this.config = fileconfig.default
      this.config.src = APP_ROOT + '/files'
    } else {
      if (cli.deploymenu === 'desa') {
        this.config = fileconfig.desa
        utils.cmdOutput(colors.cyan('OPTION DESA'))
      } else if (cli.deploymenu === 'prep') {
        this.config = fileconfig.prep
        utils.cmdOutput(colors.cyan('OPTION PREP'))
      }
    }
    this.onInit()
  }

  onInit () {
    // utils.traceEvents()
    this.emitter = new events.EventEmitter()
    this.client = new Client()
    this.host = this.config.host
    this.port = this.config.port
    this.username = this.config.username
    this.password = this.config.password
    this.filepath = this.config.path
    this.src = this.config.src
    this.client.defaults({
      host: this.host,
      username: this.username,
      password: this.password
    })
    this.onConnect()
  }

  onConnect () {
    utils.cmdOutput(
      colors.green('Connect to: ') +
      colors.green.underline(this.host + ':' + this.port)
    )
    this.uploadFile().then(() => {
      this.onSuccess()
    })
    this.addEventsListener()
  }

  uploadFile () {
    var this_ = this
    var filePath = this.src
    var extension = '.war'
    var filename
    var filesize
    var GF = getfiles(filePath, extension)
    Object.keys(GF).forEach(function (key) {
      if (GF[key] === GF['FILES']) {
        GF[key].forEach(function (fileitem, index) {
          filename = fileitem
          if (index === 0) return filename
        })
      }
      if (GF[key] === GF['SIZES']) {
        GF[key].forEach(function (fileitem, index) {
          filesize = fileitem.filesize
          if (index === 0) return filesize
        })
      }
    })
    this.filename = path.basename(filename)
    this.filesize = filesize.toFixed(2)
    return new Promise(function (resolve) {
      this_.client.upload(filename, this_.filepath + this_.filename, function (err) {
        if (err) { this_.onError(err) } else {
          return resolve()
        }
      })
    })
  }

  showProgressBar (percent, onComplete) {
    const progbar = new progressBar.Bar({
      format: colors.yellow('Progress') +
      colors.cyan(' | ') +
      colors.cyan('[{bar}]') +
      colors.cyan(' | ') +
      colors.yellow('{percentage}%') +
      colors.cyan(' | ') +
      colors.yellow('{value}/{total}'),
      barCompleteChar: '\u2588',
      barIncompleteChar: '\u2591',
      barsize: 30,
      width: 30,
      fps: 50
    })
    progbar.start(100, percent)
    if (percent < 100) {
      progbar.update(percent)
      onComplete(false)
    } else {
      onComplete(true)
      progbar.stop()
    }
  }

  onProgress (percent) {
    // console.log('​this.client.on("transfer")', this.client._events._option)
    // console.log('​this.client.on("transfer")', Object.keys(this.client))
    this.showProgressBar(percent, function (onComplete) {
      // utils.devInfo('START PROCESS PROGRESSBAR')
      if (onComplete) {
        utils.cmdOutput()
        // utils.devInfo('FINISH PROCESS PROGRESSBAR')
        // utils.cmdOutput()
      }
    })
  }

  exit () {
    this.client.close()
    process.exit()
  }

  onError (err) {
    console.log(`${err}`.red.underline.italic)
    this.exit()
  }

  onSuccess () {
    var check = emoji.find('white_check_mark')
    var data = [
      ['Name', colors.magenta(this.filename), colors.green(check.emoji)],
      ['Size', colors.magenta(this.filesize, 'MB'), colors.green(check.emoji)]
    ]
    var config = {
      border: tgbc('norc'),
      columnDefault: {
        width: 20
      },
      columnCount: 3,
      columns: {
        0: {
          width: 5
        },
        1: {
          width: 30
        },
        2: {
          width: 2,
          alignment: 'center'
        }
      }
    }
    utils.cmdOutput()    
    utils.cmdOutput(colors.cyan.dim.bold('Upload info'))
    utils.cmdOutput(colors.gray(table(data, config)))
    utils.cmdOutput()    
    cli.opts.selectopt()
  }

  addEventsListener () {
    this.client.on('connect', () => {
      return utils.devInfo('CONNECT')
    })
    this.client.on('ready', () => {
      return utils.devInfo('READY')
    })
    this.client.on('write', () => {
      return utils.devInfo('WRITE')
    })
    this.client.on('transfer', (buffer, uploaded, total) => {
      if (uploaded === 0) utils.devInfo('TRANSFER')
      var up = uploaded += 1
      var percent = Math.round((up / total) * 100)
      this.onProgress(percent)
    })
    this.client.on('close', () => {
      return utils.devInfo('CLOSE')
    })
    this.client.on('error', (err) => {
      utils.devInfo('ERROR', err)
    })
    this.client.on('end', () => {
      return utils.devInfo('END')
    })
  }
}

module.exports = function () {
  if (utils.dev) { utils.cmdOutput(colors.cyan('DEPLOY IN DEV MODE')) }
  return new RunClient()
}
